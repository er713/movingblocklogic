use pyo3::prelude::*;

pub mod board_generator;
pub mod file_io;

pub fn add_to_module(m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(generate_boards, m)?)?;
    Ok(())
}

#[pyfunction]
pub fn generate_boards(
    width: usize,
    height: usize,
    quantity: u8,
    start_id: u16,
    moves_std: (f32, f32),
    blank_probability: f64,
    folder_path: Option<&str>,
    seed: Option<u64>,
    verbose: Option<bool>,
) -> PyResult<()> {
    board_generator::generate_all(
        width,
        height,
        quantity,
        start_id,
        match folder_path {
            Some(folder) => folder,
            None => "maps",
        },
        moves_std,
        blank_probability,
        seed,
        match verbose {
            Some(v) => v,
            None => false,
        },
    );
    Ok(())
}
