use super::super::board::BoardTypeState;
use super::super::solver::get_astar_results;
use serde::{Deserialize, Serialize};
use serde_json;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

#[derive(Serialize, Deserialize)]
pub struct MapInfo {
    pub board: BoardTypeState,
    pub start_position: (i16, i16),
    pub goal_rectangle: ((i16, i16), (i16, i16)),
    pub astar_diff: u32,
    pub min_moves: u8,
}

impl MapInfo {
    pub fn new(tup: (BoardTypeState, (i16, i16), ((i16, i16), (i16, i16)))) -> Self {
        Self {
            board: tup.0,
            start_position: tup.1,
            goal_rectangle: tup.2,
            astar_diff: 0,
            min_moves: 0,
        }
    }

    pub fn save_to_file(&self, path: &Path) -> std::io::Result<()> {
        let ser = serde_json::to_string(&self).unwrap();
        let mut file = File::create(path)?;
        file.write_all(ser.as_bytes())?;
        Ok(())
    }
    pub fn set_diff(&mut self) {
        let (states, diff) = get_astar_results(
            self.board.clone(),
            self.start_position.clone(),
            self.goal_rectangle.clone(),
        );
        self.astar_diff = diff;
        self.min_moves = states.len() as u8 - 1
    }
    pub fn read_from_file(path: &Path) -> std::io::Result<Self> {
        let mut file = File::open(path)?;
        let mut s = String::new();
        file.read_to_string(&mut s)?;
        Ok(serde_json::from_str(&s).unwrap())
    }
}
