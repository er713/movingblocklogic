use super::super::board::{Board, BoardTypeState};
use super::super::enums::*;
use super::file_io::MapInfo;
use rand::distributions::{Standard, Uniform};
use rand::prelude::*;
use rand_chacha::ChaCha20Rng;
use std::path::Path;
use vec2d::{Coord, Size, Vec2D};

pub fn generate_all(
    width: usize,
    height: usize,
    quantity: u8,
    start_id: u16,
    folder_path: &str,
    moves_std: (f32, f32),
    blank_probability: f64,
    seed: Option<u64>,
    verbose: bool,
) {
    let mut rng = match seed {
        Some(x) => ChaCha20Rng::seed_from_u64(x),
        None => ChaCha20Rng::from_entropy(),
    };
    let verb = match verbose {
        true => |b: &BoardTypeState| {
            print_vec_state(b);
            println!("======================");
        },
        false => |_b: &BoardTypeState| (),
    };

    let size = Size::new(width, height);
    let b: BoardTypeState = Vec2D::from_example(size, &State::Tile);
    let mut env = Board::new(b, [0; 2], &[1, 1, 2], &[0; 4], &Some((2, 2)), false, 0, 0);
    let mut map: MapInfo;
    let mut q: u8 = 0;
    loop {
        map = MapInfo::new(generate_one(
            &mut rng,
            &mut env,
            size,
            moves_std,
            blank_probability,
        ));
        map.set_diff();
        if map.min_moves <= 1 {
            continue;
        }
        println!("moves: {} -- diff: {}", &map.min_moves, &map.astar_diff);
        map.save_to_file(&Path::new(&format!(
            "{}/map{}.json",
            folder_path,
            start_id + q as u16
        )))
        .unwrap();
        verb(&map.board); // printin map
        q += 1;
        if q >= quantity {
            break;
        }
    }
}

fn generate_one(
    rng: &mut ChaCha20Rng,
    env: &mut Board,
    size: Size,
    moves_std: (f32, f32),
    blank_probability: f64,
) -> (BoardTypeState, (i16, i16), ((i16, i16), (i16, i16))) {
    let mut b = Vec2D::from_example(size, &State::Tile);
    let start_pos = (
        rng.gen_range(2..(size.height as i16 + 2)),
        rng.gen_range(2..(size.width as i16 + 2)),
    );
    env.set_start_pos(start_pos.0, start_pos.1);
    env.mark_player_state(&mut b, &(2, 2));
    let mut last_action: Action = Action::from_u8(rng.gen_range(1..=4)).unwrap();
    let zeroth_action = last_action.clone();
    let possible_action: Vec<Action> = vec![1, 2, 3, 4u8]
        .iter()
        .map(|v| Action::from_u8(*v).unwrap())
        .collect();
    let mut rand_action: Vec<Action>;
    let moves_qt = ((rng.sample::<f32, _>(Standard) * moves_std.1 + moves_std.0) as i16)
        .clamp(1, i16::MAX) as u8;
    let mut steps = 0;
    // Generete solution path
    loop {
        rand_action = possible_action.clone();
        rand_action.retain(|v| v != &Action::oposit(&last_action));

        loop {
            if rand_action.is_empty() {
                if steps == 0 {
                    last_action = zeroth_action.clone();
                    env.action(&last_action);
                    if env.player_on_blank() {
                        env.mark_player_state(&mut b, &(2, 2));
                        panic!("Wrong last move");
                    }
                    break;
                } else {
                    env.mark_player_state(&mut b, &(2, 2));
                    panic!("Empty posible action");
                }
            }
            last_action = rand_action[rng.gen_range(0..rand_action.len())].clone();
            env.action(&last_action);
            // Check if not outsite board
            if env.player_on_blank() {
                env.action(&last_action.oposit());
                rand_action.retain(|v| v != &last_action);
                continue;
            }
            break;
        }

        env.mark_player_state(&mut b, &(2, 2)); // marking player position
        steps += 1;

        if moves_qt <= steps {
            break;
        }
    }
    let goal_r = env.get_player_rectangle();
    let goal_rect = goal_r.to_rect().unwrap();
    let m = Coord::new(2, 2);
    // Remove random tiles and change Player to Tile
    let unif = Uniform::new(0f64, 1f64);
    for (c, v) in b.iter_mut() {
        match v {
            State::PlayerSite1 => *v = State::Tile,
            _ => {
                if rng.sample::<f64, _>(unif) < blank_probability {
                    *v = State::Blank;
                }
            }
        }
        if goal_rect.contains_coord(c + m) {
            *v = State::Goal;
        }
    }
    (
        b.clone(),
        (start_pos.0 - 2, start_pos.1 - 2),
        (
            (goal_r.closer[0] - 2, goal_r.closer[1] - 2),
            (goal_r.farer[0] - 2, goal_r.farer[1] - 2),
        ),
    )
}

pub fn print_vec_state(board: &BoardTypeState) {
    let s = board.size();
    for i in 0..s.height {
        for j in 0..s.width {
            print!("{} ", board.get(Coord::new(j, i)).unwrap());
        }
        print!("\n");
    }
}
