use super::board::{Board, BoardTypeState, Surrounding};
use super::enums::*;
use vec2d::Vec2D;

pub trait GymEnv<B, D> {
    fn step(
        &mut self,
        action: u8,
        fragment: Option<&Surrounding>,
    ) -> ((B, D), f32, bool, Option<String>);
    fn reset(&mut self, fragment: Option<&Surrounding>, _seed: Option<u8>) -> (B, D);
}

trait GymRenderedEnv<B, D>: GymEnv<B, D> {
    fn render(&self) -> ();
}

pub struct MovingBlockAll {
    board: Board,
    max_steps: u16,
    steps: u16,
}

impl MovingBlockAll {
    pub fn new(
        board: BoardTypeState,
        position: [i16; 2],
        sizes: &[i16; 3],
        goal: &[i16; 4],
        margin: &Option<Surrounding>,
        difficulty: u32,
        min_moves: u8,
    ) -> Self {
        MovingBlockAll {
            board: Board::new(
                board, position, sizes, goal, margin, true, difficulty, min_moves,
            ),
            max_steps: 360,
            steps: 0,
        }
    }
    pub fn get_difficult(&self) -> &u32 {
        self.board.get_difficulty()
    }
    pub fn get_min_moves(&self) -> &u8 {
        self.board.get_min_moves()
    }
}

impl GymEnv<Vec<Vec<u8>>, [i16; 2]> for MovingBlockAll {
    fn step(
        &mut self,
        action: u8,
        fragment: Option<&Surrounding>,
    ) -> ((Vec<Vec<u8>>, [i16; 2]), f32, bool, Option<String>) {
        let d = self.board.distance();
        if self.steps >= self.max_steps {
            return (
                (vec2d_to_vec(self.board.board_to_vec(fragment)), d),
                (500 - (d[0].abs() + d[1].abs())) as f32,
                true,
                None,
            );
        }
        self.board.action(&Action::from_u8(action).unwrap());
        self.steps += 1;
        let mut b = self.board.board_to_vec(fragment);
        self.board.mark_player_position(&mut b, fragment);
        let d = self.board.distance();

        if self.board.check_goal_reached() {
            (
                (vec2d_to_vec(b), d),
                (self.max_steps - self.steps) as f32 + 1000_f32,
                true,
                None,
            )
        } else if self.board.player_on_blank() {
            ((vec2d_to_vec(b), d), -100_f32, true, None)
        } else {
            ((vec2d_to_vec(b), d), 1_f32, false, None)
        }
    }
    fn reset(
        &mut self,
        fragment: Option<&Surrounding>,
        _seed: Option<u8>,
    ) -> (Vec<Vec<u8>>, [i16; 2]) {
        self.board.reset();
        self.steps = 0;
        let mut b = self.board.board_to_vec(fragment);
        self.board.mark_player_position(&mut b, fragment);
        let d = self.board.distance();
        (vec2d_to_vec(b), d)
    }
}

fn vec2d_to_vec(v: Vec2D<u8>) -> Vec<Vec<u8>> {
    let s = v.size();
    let mut res = vec![vec![0u8; s.width]; s.height];
    v.iter().for_each(|(c, r)| res[c.y][c.x] = *r);
    res
}

// fn vec2d_to_vecs(v: Vec2D<State>) -> Vec<Vec<State>> {
//     let s = v.size();
//     let mut res = vec![vec![State::Blank; s.width]; s.height];
//     v.iter().for_each(|(c, r)| res[c.y][c.x] = *r);
//     res
// }
