use serde::{Deserialize, Serialize};
use std::cmp::PartialEq;
use std::fmt::{Display, Formatter};

#[derive(Clone, Debug)]
pub enum Action {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
pub enum State {
    Blank,
    Tile,
    Goal,
    PlayerSite1,
    PlayerSite2,
    PlayerSite3,
}

impl State {
    pub fn to_u8(&self) -> u8 {
        match self {
            State::Blank => 0,
            State::Tile => 1,
            State::Goal => 5,
            State::PlayerSite1 => 2,
            State::PlayerSite2 => 3,
            State::PlayerSite3 => 4,
        }
    }
}

impl Action {
    pub fn oposit(&self) -> Action {
        match self {
            Action::Up => Action::Down,
            Action::Down => Action::Up,
            Action::Left => Action::Right,
            Action::Right => Action::Left,
        }
    }

    pub fn from_u8(action: u8) -> Result<Action, String> {
        match action {
            1 => Ok(Action::Up),
            2 => Ok(Action::Right),
            3 => Ok(Action::Down),
            4 => Ok(Action::Left),
            0 | 5.. => Err(String::from("Wrong action")),
        }
    }
    pub fn to_u8(&self) -> u8 {
        match self {
            Action::Up => 1,
            Action::Right => 2,
            Action::Down => 3,
            Action::Left => 4,
        }
    }
    // pub fn to_usize(&self) -> usize {
    //     match self {
    //         Action::Up => 1,
    //         Action::Right => 2,
    //         Action::Down => 3,
    //         Action::Left => 4,
    //     }
    // }
}

impl PartialEq for Action {
    fn eq(&self, other: &Self) -> bool {
        self.to_u8() == other.to_u8()
    }
}

impl Display for Action {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Action::Up => "Up",
                Action::Right => "Right",
                Action::Down => "Down",
                Action::Left => "Left",
            }
        )
    }
}

impl Display for State {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                State::Blank => "O",
                State::Tile => "T",
                State::Goal => "G",
                State::PlayerSite1 => "1",
                State::PlayerSite2 => "2",
                State::PlayerSite3 => "3",
            }
        )
    }
}
