use pyo3::prelude::*;
mod board;
mod cuboid;
mod enums;
pub mod generator;
mod gym_api;
pub mod python_wrapping;
mod rectangle;
pub mod solver;
use generator::add_to_module as gen_add_to_module;
use python_wrapping::*;
use solver::add_to_module as sol_add_to_module;

pub fn add_to_module(m: &PyModule) -> PyResult<()> {
    m.add_class::<MovingBlockAll>().unwrap();
    m.add_class::<MovingBlockSurrounding>().unwrap();
    m.add_class::<MovingBlockDirected>().unwrap();
    gen_add_to_module(m).unwrap();
    sol_add_to_module(m).unwrap();
    Ok(())
}
