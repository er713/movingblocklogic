use super::board::Surrounding;
use super::generator::file_io::MapInfo;
use super::gym_api::{GymEnv, MovingBlockAll as MvBlAll};
use pyo3::prelude::*;
use pyo3::types::PyType;
use std::cmp::max;
use std::path::Path;

#[pyclass]
pub struct MovingBlockAll {
    moving_block: MvBlAll,
}

#[pymethods]
impl MovingBlockAll {
    // #[new]
    // pub fn new() -> Self {
    //     MovingBlockAll {
    //         moving_block: MvBlAll::new([2, 2], &[1, 1, 2], &[3, 4, 5, 5], &Some((1, 1))),
    //     }
    // }

    #[classmethod]
    pub fn from_file(_t: &PyType, path: &str) -> Self {
        let map = MapInfo::read_from_file(&Path::new(path)).unwrap();
        Self {
            moving_block: MvBlAll::new(
                map.board,
                [map.start_position.0, map.start_position.1],
                &[1, 1, 2],
                &[
                    map.goal_rectangle.0 .0,
                    map.goal_rectangle.0 .1,
                    map.goal_rectangle.1 .0,
                    map.goal_rectangle.1 .1,
                ],
                &Some((2, 2)),
                map.astar_diff,
                map.min_moves,
            ),
        }
    }

    #[getter]
    pub fn difficulty(&self) -> PyResult<u32> {
        Ok(*self.moving_block.get_difficult())
    }
    #[getter]
    pub fn min_moves(&self) -> PyResult<u8> {
        Ok(*self.moving_block.get_min_moves())
    }

    pub fn step(
        &mut self,
        action: u8,
    ) -> PyResult<((Vec<Vec<u8>>, u16), f32, bool, Option<String>)> {
        let ((s, d), r, e, _) = self.moving_block.step(action, None);
        Ok(((s, vec2distance(d)), r, e, None))
    }

    pub fn reset(&mut self, _seed: Option<u8>) -> PyResult<(Vec<Vec<u8>>, u16)> {
        let (s, d) = self.moving_block.reset(None, _seed);
        Ok((s, vec2distance(d)))
    }
}

fn vec2distance(tab: [i16; 2]) -> u16 {
    (tab[0].abs() + tab[1].abs()) as u16
}

#[pyclass]
pub struct MovingBlockSurrounding {
    moving_block: MvBlAll,
    surrounding: Surrounding,
}

#[pymethods]
impl MovingBlockSurrounding {
    // #[new]
    // pub fn new(width: u8, height: u8) -> Self {
    //     MovingBlockSurrounding {
    //         moving_block: MvBlAll::new([2, 2], &[1, 1, 2], &[3, 4, 5, 5], &Some((2, 2))),
    //         surrounding: (height, width),
    //     }
    // }

    #[classmethod]
    pub fn from_file(_t: &PyType, path: &str, surrounding: (u8, u8)) -> Self {
        let map = MapInfo::read_from_file(&Path::new(path)).unwrap();
        Self {
            moving_block: MvBlAll::new(
                map.board,
                [map.start_position.0, map.start_position.1],
                &[1, 1, 2],
                &[
                    map.goal_rectangle.0 .0,
                    map.goal_rectangle.0 .1,
                    map.goal_rectangle.1 .0,
                    map.goal_rectangle.1 .1,
                ],
                &Some((max(surrounding.0 + 2, 2), max(surrounding.1 + 2, 2))),
                map.astar_diff,
                map.min_moves,
            ),
            surrounding: surrounding,
        }
    }

    #[getter]
    pub fn difficulty(&self) -> PyResult<u32> {
        Ok(*self.moving_block.get_difficult())
    }
    #[getter]
    pub fn min_moves(&self) -> PyResult<u8> {
        Ok(*self.moving_block.get_min_moves())
    }

    pub fn step(
        &mut self,
        action: u8,
    ) -> PyResult<((Vec<Vec<u8>>, u16), f32, bool, Option<String>)> {
        let ((s, d), r, e, _) = self.moving_block.step(action, Some(&self.surrounding));
        Ok(((s, vec2distance(d)), r, e, None))
    }

    pub fn reset(&mut self, _seed: Option<u8>) -> PyResult<(Vec<Vec<u8>>, u16)> {
        let (s, d) = self.moving_block.reset(Some(&self.surrounding), _seed);
        Ok((s, vec2distance(d)))
    }
}

#[pyclass]
pub struct MovingBlockDirected {
    moving_block: MvBlAll,
    surrounding: Surrounding,
}

#[pymethods]
impl MovingBlockDirected {
    // #[new]
    // pub fn new(width: u8, height: u8) -> Self {
    //     MovingBlockSurrounding {
    //         moving_block: MvBlAll::new([2, 2], &[1, 1, 2], &[3, 4, 5, 5], &Some((2, 2))),
    //         surrounding: (height, width),
    //     }
    // }

    #[classmethod]
    pub fn from_file(_t: &PyType, path: &str, surrounding: (u8, u8)) -> Self {
        let map = MapInfo::read_from_file(&Path::new(path)).unwrap();
        Self {
            moving_block: MvBlAll::new(
                map.board,
                [map.start_position.0, map.start_position.1],
                &[1, 1, 2],
                &[
                    map.goal_rectangle.0 .0,
                    map.goal_rectangle.0 .1,
                    map.goal_rectangle.1 .0,
                    map.goal_rectangle.1 .1,
                ],
                &Some((max(surrounding.0 + 4, 2), max(surrounding.1 + 4, 2))),
                map.astar_diff,
                map.min_moves,
            ),
            surrounding: surrounding,
        }
    }

    #[getter]
    pub fn difficulty(&self) -> PyResult<u32> {
        Ok(*self.moving_block.get_difficult())
    }
    #[getter]
    pub fn min_moves(&self) -> PyResult<u8> {
        Ok(*self.moving_block.get_min_moves())
    }

    pub fn step(
        &mut self,
        action: u8,
    ) -> PyResult<((Vec<Vec<u8>>, [i16; 2]), f32, bool, Option<String>)> {
        Ok(self.moving_block.step(action, Some(&self.surrounding)))
    }

    pub fn reset(&mut self, _seed: Option<u8>) -> PyResult<(Vec<Vec<u8>>, [i16; 2])> {
        Ok(self.moving_block.reset(Some(&self.surrounding), _seed))
    }
}
