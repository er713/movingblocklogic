use super::super::board::{BoardTypeState, Surrounding};
use super::super::cuboid::Cuboid;
use super::super::enums::*;
use super::super::rectangle::Rectangle;
use std::cmp::{Eq, PartialEq};
use std::hash::{Hash, Hasher};
use std::rc::Rc;
use vec2d::*;

const ALL_ACTION: [Action; 4] = [Action::Up, Action::Right, Action::Down, Action::Left];
pub static mut CL_IT: u32 = 0;

#[derive(Clone, Debug)]
pub struct BoardSolv {
    cuboid: Cuboid,
    board: Rc<BoardTypeState>,
    goal: Rc<Rectangle>,
    pub action_: Option<Action>,
}

impl BoardSolv {
    pub fn new(
        board: BoardTypeState,
        position: [i16; 2],
        sizes: &[i16; 3],
        goal: &[i16; 4],
        margin: &Option<Surrounding>,
        check: bool,
    ) -> Self {
        unsafe { CL_IT = 0 }; //only one threat so ok
        let g;
        let b;
        let p;
        match margin {
            Some((m1, m2)) => {
                g = Rectangle::new(
                    goal[0] + *m1 as i16,
                    goal[1] + *m2 as i16,
                    goal[2] + *m1 as i16,
                    goal[3] + *m2 as i16,
                );
                p = [position[0] + *m1 as i16, position[1] + *m2 as i16];
                b = Self::add_margin(board, (*m1, *m2))
            }
            None => {
                g = Rectangle::new(goal[0], goal[1], goal[2], goal[3]);
                p = position;
                b = board;
            }
        }
        if check {
            Self::check_board_goal(&b, &g)
        };
        Self {
            cuboid: Cuboid::new(&p, sizes),
            board: Rc::new(b),
            goal: Rc::new(g),
            action_: None,
        }
    }

    pub fn action(&mut self, action: &Action) {
        self.cuboid.make_move(action);
    }
    pub fn check_goal_reached(&self) -> bool {
        self.cuboid.to_rectangle() == self.goal.as_ref()
    }

    pub fn player_on_blank(&self) -> bool {
        let player: &Rectangle = self.cuboid.to_rectangle();
        match player.to_rect() {
            Some(rect) => match self.board.rect_iter(rect) {
                Some(r) => {
                    for (_, v) in r {
                        match v {
                            State::Blank => return true,
                            _ => (),
                        };
                    }
                }
                None => return true,
            },
            None => return true,
        }
        false
    }
    pub fn distance(&self) -> u16 {
        let t = self.cuboid.to_rectangle().distance(&self.goal);
        (t[0].abs() + t[1].abs()) as u16
    }
    pub fn next_states(&self) -> Vec<(Self, u16)> {
        unsafe { CL_IT += 1 };
        let mut nx = Vec::<(Self, u16)>::new();
        let mut b = self.clone();
        for action in ALL_ACTION.iter() {
            b.action(action);
            if !self.player_on_blank() {
                b.action_ = Some(action.clone());
                nx.push((b.clone(), 1));
            }
            b.action(&action.oposit());
        }
        nx
    }

    fn add_margin(board: BoardTypeState, margin: Surrounding) -> BoardTypeState {
        let s = board.size();
        let begin = Coord::new(margin.0 as usize, margin.1 as usize);
        let mut new_b = BoardTypeState::from_example(
            Size::new(
                s.width + 2 * margin.0 as usize,
                s.height + 2 * margin.1 as usize,
            ),
            &State::Blank,
        );
        new_b
            .rect_iter_mut(
                Rect::new(
                    begin.clone(),
                    Coord::new(
                        s.width - 1 + margin.0 as usize,
                        s.height - 1 + margin.1 as usize,
                    ),
                )
                .unwrap(),
            )
            .unwrap()
            .for_each(|(c, v)| *v = *board.get(Self::subtract_coords(c, begin)).unwrap());
        new_b
    }

    fn check_board_goal(board: &BoardTypeState, goal: &Rectangle) {
        for (_, v) in board.rect_iter(goal.to_rect().unwrap()).unwrap() {
            match v {
                State::Goal => (),
                _ => panic!["Specified Goal do not match with board"],
            }
        }
    }

    fn subtract_coords(c1: Coord, c2: Coord) -> Coord {
        Coord::new(c1.x - c2.x, c1.y - c2.y)
    }

    pub fn set_start_pos(&mut self, p0: i16, p1: i16) {
        self.cuboid.set_pos(p0, p1)
    }
    pub fn print_player_pos(&self) {
        println!("pos: {}", self.cuboid.to_rectangle());
    }
}

impl PartialEq for BoardSolv {
    fn eq(&self, other: &Self) -> bool {
        self.cuboid == other.cuboid
    }
}

impl Eq for BoardSolv {}

impl Hash for BoardSolv {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cuboid.to_rectangle().hash(state)
    }
}
