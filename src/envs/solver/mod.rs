use super::board::BoardTypeState;
use super::generator::file_io::MapInfo;
use pf::prelude::astar;
use pyo3::prelude::*;
use std::path::Path;
pub mod board_solv;
use board_solv::{BoardSolv, CL_IT};

pub fn add_to_module(m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(run_astar, m)?)?;
    Ok(())
}

/// Do NOT run parallel
pub fn get_astar_results(
    board: BoardTypeState,
    start_position: (i16, i16),
    goal_rectangle: ((i16, i16), (i16, i16)),
) -> (Vec<u8>, u32) {
    let mut b = BoardSolv::new(
        board,
        [start_position.0, start_position.1],
        &[1, 1, 2],
        &[
            goal_rectangle.0 .0,
            goal_rectangle.0 .1,
            goal_rectangle.1 .0,
            goal_rectangle.1 .1,
        ],
        &Some((2, 2)),
        true,
    );
    let (result, _cost) = astar(
        &mut b,
        |b| b.next_states(),
        |b| b.distance(),
        |b| b.check_goal_reached(),
    )
    .unwrap();
    let cl;
    unsafe { cl = CL_IT }; // Only one thread
    (
        result[1..]
            .iter()
            .map(|b| match &b.action_ {
                Some(a) => a.to_u8(),
                None => 0,
            })
            .collect(),
        cl,
    )
}

#[pyfunction]
pub fn run_astar(path: &str) -> PyResult<(Vec<u8>, u32)> {
    let map = MapInfo::read_from_file(Path::new(path)).unwrap();
    Ok(get_astar_results(
        map.board,
        map.start_position,
        map.goal_rectangle,
    ))
}
