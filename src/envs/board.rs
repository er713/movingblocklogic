use super::cuboid::Cuboid;
use super::enums::*;
// use super::generator::board_generator::print_vec_state;
use super::rectangle::Rectangle;
use vec2d::*;

pub type BoardTypeState = Vec2D<State>;
pub type BoardTypeInt = Vec2D<u8>;
pub type Surrounding = (u8, u8);

pub struct Board {
    cuboid: Cuboid,
    board: BoardTypeState,
    goal: Rectangle,
    difficulty: u32,
    min_moves: u8,
}

impl Board {
    /// Constructor TODO: read board from list of generated/prepared ones
    pub fn new(
        board: BoardTypeState,
        position: [i16; 2],
        sizes: &[i16; 3],
        goal: &[i16; 4],
        margin: &Option<Surrounding>,
        check: bool,
        difficulty: u32,
        min_moves: u8,
    ) -> Self {
        let g;
        let b;
        let p;
        match margin {
            Some((m1, m2)) => {
                g = Rectangle::new(
                    goal[0] + *m1 as i16,
                    goal[1] + *m2 as i16,
                    goal[2] + *m1 as i16,
                    goal[3] + *m2 as i16,
                );
                p = [position[0] + *m1 as i16, position[1] + *m2 as i16];
                b = Self::add_margin(board, (*m1, *m2))
            }
            None => {
                g = Rectangle::new(goal[0], goal[1], goal[2], goal[3]);
                p = position;
                b = board;
            }
        }
        if check {
            // print_vec_state(&b);
            Self::check_board_goal(&b, &g)
        };
        Board {
            cuboid: Cuboid::new(&p, sizes),
            board: b,
            goal: g,
            difficulty: difficulty,
            min_moves: min_moves,
        }
    }

    pub fn action(&mut self, action: &Action) {
        self.cuboid.make_move(action);
    }
    pub fn check_goal_reached(&self) -> bool {
        self.cuboid.to_rectangle() == &self.goal
    }
    pub fn get_difficulty(&self) -> &u32 {
        &self.difficulty
    }
    pub fn get_min_moves(&self) -> &u8 {
        &self.min_moves
    }

    pub fn player_on_blank(&self) -> bool {
        let player: &Rectangle = self.cuboid.to_rectangle();
        match player.to_rect() {
            Some(rect) => match self.board.rect_iter(rect) {
                Some(r) => {
                    for (_, v) in r {
                        match v {
                            State::Blank => return true,
                            _ => (),
                        };
                    }
                }
                None => return true,
            },
            None => return true,
        }
        false
    }

    pub fn reset(&mut self) {
        self.cuboid.reset();
    }
    pub fn distance(&self) -> [i16; 2] {
        self.cuboid.to_rectangle().distance(&self.goal)
    }
    pub fn get_player_rectangle(&self) -> &Rectangle {
        self.cuboid.to_rectangle()
    }

    pub fn board_to_vec(&self, scope: Option<&Surrounding>) -> BoardTypeInt {
        let rec = self.cuboid.to_rectangle();
        match scope {
            Some((r1, r2)) => {
                let x_m: i16 = (rec.farer[0] - rec.closer[0]) / 2;
                let y_m: i16 = (rec.farer[1] - rec.closer[1]) / 2;
                let rect = Rect::new(
                    Coord::new(
                        (rec.closer[1] + y_m - *r2 as i16) as usize,
                        (rec.closer[0] + x_m - *r1 as i16) as usize,
                    ),
                    Coord::new(
                        (rec.closer[1] + y_m + *r2 as i16) as usize,
                        (rec.closer[0] + x_m + *r1 as i16) as usize,
                    ),
                )
                .unwrap();
                // println!("{:?}, {:?}", rect, self.board.size());
                // println!("{:?}", self.board);
                Vec2D::from_vec(
                    rect.size(),
                    self.board
                        .rect_iter(rect)
                        .unwrap()
                        .map(|(_, v)| v.to_u8())
                        .collect::<Vec<u8>>(),
                )
                .unwrap()
            }
            None => Vec2D::from_vec(
                self.board.size(),
                self.board.iter().map(|(_, v)| v.to_u8()).collect(),
            )
            .unwrap(),
        }
    }
    pub fn mark_player_state(&self, board: &mut BoardTypeState, margin: &Surrounding) {
        let player = self.cuboid.to_rectangle();
        // println!("{}", player);
        for (_, v) in board
            .rect_iter_mut(
                Rect::new(
                    Coord::new(
                        (player.closer[1] - margin.1 as i16) as usize,
                        (player.closer[0] - margin.0 as i16) as usize,
                    ),
                    Coord::new(
                        ((player.farer[1] - margin.1 as i16) - 1) as usize,
                        ((player.farer[0] - margin.0 as i16) - 1) as usize,
                    ),
                )
                .unwrap(),
            )
            .unwrap()
        {
            *v = State::PlayerSite1;
        }
    }
    pub fn mark_player_position_st(
        board: &mut BoardTypeInt,
        surr: Option<&Surrounding>,
        state: State,
        player: &Rectangle,
    ) {
        match surr {
            None => {
                for (_, v) in board.rect_iter_mut(player.to_rect().unwrap()).unwrap() {
                    *v = state.to_u8();
                }
            }
            Some((r1, r2)) => {
                let x_d: i16 = player.farer[0] - player.closer[0];
                let y_d: i16 = player.farer[1] - player.closer[1];
                let x_m: f32 = x_d as f32 / 2_f32;
                let y_m: f32 = y_d as f32 / 2_f32;
                for (_, v) in board
                    .rect_iter_mut(
                        Rect::new(
                            Coord::new(
                                (*r2 as i16 - (y_d - y_m.ceil() as i16)) as usize,
                                (*r1 as i16 - (x_d - x_m.ceil() as i16)) as usize,
                            ),
                            Coord::new(
                                (*r2 as i16 + (y_d - y_m.floor() as i16) - 1) as usize,
                                (*r1 as i16 + (x_d - x_m.floor() as i16) - 1) as usize,
                            ),
                        )
                        .unwrap(),
                    )
                    .unwrap()
                {
                    *v = state.to_u8();
                }
            }
        }
    }
    pub fn mark_player_position(&self, board: &mut BoardTypeInt, surr: Option<&Surrounding>) {
        Self::mark_player_position_st(
            board,
            surr,
            self.cuboid.get_site().unwrap(),
            self.cuboid.to_rectangle(),
        )
    }

    fn add_margin(board: BoardTypeState, margin: Surrounding) -> BoardTypeState {
        let s = board.size();
        let begin = Coord::new(margin.0 as usize, margin.1 as usize);
        let mut new_b = BoardTypeState::from_example(
            Size::new(
                s.width + 2 * margin.0 as usize,
                s.height + 2 * margin.1 as usize,
            ),
            &State::Blank,
        );
        new_b
            .rect_iter_mut(
                Rect::new(
                    begin.clone(),
                    Coord::new(
                        s.width - 1 + margin.0 as usize,
                        s.height - 1 + margin.1 as usize,
                    ),
                )
                .unwrap(),
            )
            .unwrap()
            .for_each(|(c, v)| *v = *board.get(Self::subtract_coords(c, begin)).unwrap());
        new_b
    }

    fn check_board_goal(board: &BoardTypeState, goal: &Rectangle) {
        for (_, v) in board.rect_iter(goal.to_rect().unwrap()).unwrap() {
            match v {
                State::Goal => (),
                _ => panic!["Specified Goal do not match with board"],
            }
        }
    }

    fn subtract_coords(c1: Coord, c2: Coord) -> Coord {
        Coord::new(c1.x - c2.x, c1.y - c2.y)
    }

    pub fn set_start_pos(&mut self, p0: i16, p1: i16) {
        self.cuboid.set_pos(p0, p1)
    }

    #[allow(dead_code)]
    pub fn print_player_pos(&self) {
        println!("pos: {}", self.cuboid.to_rectangle());
    }
}
