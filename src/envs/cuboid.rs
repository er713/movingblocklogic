use super::enums::*;
use super::rectangle::Rectangle;
use na::{matrix, Matrix4, Point2, Translation3};
use std::cmp::{max, min, PartialEq};

#[derive(Clone, Debug)]
pub struct Cuboid {
    position: Matrix4<i16>,
    start_position: Matrix4<i16>,
    rectangle: Rectangle,
    _translation: Translation3<i16>,
}

impl Cuboid {
    pub fn new(position: &[i16; 2], sizes: &[i16; 3]) -> Self {
        let pos = matrix![
            sizes[0], 0, 0, position[0];
            0, sizes[1], 0, position[1];
            0, 0, sizes[2], 0;
            0, 0, 0, 1;
        ];
        Cuboid {
            position: pos,
            start_position: pos.clone(),
            rectangle: Rectangle {
                closer: Point2::<i16>::new(position[0], position[1]),
                farer: Point2::<i16>::new(position[0] + sizes[0], position[1] + sizes[1]),
            },
            _translation: Translation3::new(0, 0, 0),
        }
    }
    pub fn make_move(&mut self, action: &Action) {
        match action {
            Action::Up => self.move_up(),
            Action::Down => self.move_down(),
            Action::Left => self.move_left(),
            Action::Right => self.move_right(),
        }
    }
    pub fn to_rectangle(&self) -> &Rectangle {
        &self.rectangle
    }
    pub fn reset(&mut self) {
        self.position = self.start_position.clone();
        self.normalize_position();
    }
    pub fn get_site(&self) -> Result<State, String> {
        if self.position[(2, 0)] != 0 {
            Ok(State::PlayerSite1)
        } else if self.position[(2, 1)] != 0 {
            Ok(State::PlayerSite2)
        } else if self.position[(2, 2)] != 0 {
            Ok(State::PlayerSite3)
        } else {
            Err(String::from("Cannot specify site of cuboid"))
        }
    }

    fn move_up(&mut self) {
        self.move_translation_rotation(self.rectangle.closer[0], 0, 0, RY_CLOSE);
    }
    fn move_down(&mut self) {
        self.move_translation_rotation(self.rectangle.farer[0], 0, 0, RY_FAR);
    }
    fn move_left(&mut self) {
        self.move_translation_rotation(0, self.rectangle.closer[1], 0, RX_CLOSE);
    }
    fn move_right(&mut self) {
        self.move_translation_rotation(0, self.rectangle.farer[1], 0, RX_FAR);
    }
    fn move_translation_rotation(&mut self, t0: i16, t1: i16, t2: i16, rotation: &Matrix4<i16>) {
        self._translation = Translation3::<i16>::new(t0, t1, t2);
        let t_matrix = self._translation.to_homogeneous();
        self._translation.inverse_mut();
        self.position = t_matrix * rotation * self._translation.to_homogeneous() * self.position;
        self.normalize_position();
    }
    fn normalize_position(&mut self) {
        (self.rectangle.closer[0], self.rectangle.closer[1]) = (
            min(self.position[(0, 3)], self.position.row(0).sum()),
            min(self.position[(1, 3)], self.position.row(1).sum()),
        );
        (self.rectangle.farer[0], self.rectangle.farer[1]) = (
            max(self.position[(0, 3)], self.position.row(0).sum()),
            max(self.position[(1, 3)], self.position.row(1).sum()),
        );
    }
    pub fn set_pos(&mut self, p0: i16, p1: i16) {
        self.position = self.start_position;
        self.position[(0, 3)] = p0;
        self.position[(1, 3)] = p1;
        self.position[(2, 3)] = 0;
        self.start_position = self.position.clone();
        self.normalize_position();
    }
}

impl PartialEq for Cuboid {
    fn eq(&self, other: &Self) -> bool {
        self.position == other.position
    }
}

// fn _id(a: usize, b: usize) -> (usize, usize) {
//     (b, a)
// }

// Ry(90)
const RY_FAR: &'static Matrix4<i16> = &matrix![
    0, 0, 1, 0;
    0, 1, 0, 0;
    -1, 0, 0, 0;
    0, 0, 0, 1
];

// Ry(-90)
const RY_CLOSE: &'static Matrix4<i16> = &matrix![
    0, 0, -1, 0;
    0, 1, 0, 0;
    1, 0, 0, 0;
    0, 0, 0, 1;
];

// Rx(-90)
const RX_FAR: &'static Matrix4<i16> = &matrix![
    1, 0, 0, 0;
    0, 0, 1, 0;
    0, -1, 0, 0;
    0, 0, 0, 1;
];

// Rx(90)
const RX_CLOSE: &'static Matrix4<i16> = &matrix![
    1, 0, 0, 0;
    0, 0, -1, 0;
    0, 1, 0, 0;
    0, 0, 0, 1;
];
