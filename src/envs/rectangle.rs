use na::Point2;
use std::cmp::PartialEq;
use std::fmt::{Display, Formatter};
use vec2d::{Coord, Rect};

#[derive(Clone, Debug, Hash)]
pub struct Rectangle {
    pub closer: Point2<i16>,
    pub farer: Point2<i16>,
}

impl Rectangle {
    pub fn new(a: i16, b: i16, x: i16, y: i16) -> Self {
        Rectangle {
            closer: Point2::new(a, b),
            farer: Point2::new(x, y),
        }
    }
    // pub fn intersecting(&self, rect: &Rectangle) -> bool {
    //     ((self.closer[0] <= rect.closer[0] && rect.closer[0] < self.farer[0])
    //         && (self.closer[1] <= rect.closer[1] && rect.closer[1] < self.farer[1]))
    //         || ((self.closer[0] < rect.farer[0] && rect.farer[0] <= self.farer[0])
    //             && (self.closer[1] < rect.farer[1] && rect.farer[1] <= self.farer[1]))
    // }
    pub fn distance(&self, rect: &Rectangle) -> [i16; 2] {
        // Self::point_distance(&self.closer, &rect.closer)
        //     + Self::point_distance(&self.farer, &rect.farer)
        [
            (rect.closer[0] - self.closer[0]) + (rect.farer[0] - self.farer[0]),
            (rect.closer[1] - self.closer[1]) + (rect.farer[1] - self.farer[1]),
        ]
    }

    #[allow(dead_code)]
    /// Manhatan distance
    fn point_distance(point1: &Point2<i16>, point2: &Point2<i16>) -> u16 {
        ((point1[0] as i32 - point2[0] as i32).abs() + (point1[1] as i32 - point2[1] as i32).abs())
            as u16
    }
    pub fn to_rect(&self) -> Option<Rect> {
        // println!("{}", &self);
        Rect::new(
            Coord::new(
                self.closer[1].clamp(0, i16::MAX) as usize,
                self.closer[0].clamp(0, i16::MAX) as usize,
            ),
            Coord::new(
                self.farer[1].clamp(1, i16::MAX) as usize - 1,
                self.farer[0].clamp(1, i16::MAX) as usize - 1,
            ),
        )
    }
}

impl PartialEq<Rectangle> for Rectangle {
    fn eq(&self, other: &Rectangle) -> bool {
        self.closer == other.closer && self.farer == other.farer
    }
    fn ne(&self, other: &Rectangle) -> bool {
        !self.eq(other)
    }
}

impl Display for Rectangle {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "({} {}) -- ({} {})",
            self.closer[0], self.closer[1], self.farer[0], self.farer[1]
        )
    }
}
