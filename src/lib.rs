// #[macro_use]
extern crate nalgebra as na;
extern crate pathfinding as pf;
use pyo3::prelude::*;

pub mod envs;
use envs::add_to_module;

#[pymodule]
fn moving_block_logic(_py: Python, m: &PyModule) -> PyResult<()> {
    add_to_module(m).unwrap();
    Ok(())
}

#[cfg(test)]
mod tests {
    extern crate nalgebra as na;
    use super::envs::generator::board_generator::generate_all;

    #[test]
    fn test_generator() {
        generate_all(10, 5, 50, 0, "dom", (9., 1.), 0.5, None, true);
    }

    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
